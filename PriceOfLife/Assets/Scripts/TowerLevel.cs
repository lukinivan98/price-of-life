﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TowerLevel : MonoBehaviour
{
    public int levelNumber = 0;
    float levelHeight = 6;

    public void SetLevel(int level)
    {
        levelHeight = transform.localScale.y * 2;
        levelNumber = level;
        transform.position = new Vector3(0f, levelHeight / 2 + levelNumber * levelHeight, 0f);
    }

    // Update is called once per frame
    void Update()
    {
        
    }
}
