﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Tower : MonoBehaviour
{
    public List<TowerLevel> towerLevels;
    public TowerLevel towerLevelPrefab;

    public void Init()
    {
        for (int i = 0; i < 4; i++)
        {
            TowerLevel towerLevel = Instantiate<TowerLevel>(towerLevelPrefab);
            towerLevel.transform.SetParent(this.transform);
            towerLevel.SetLevel(i);
            towerLevel.levelNumber = i;
            towerLevels.Add(towerLevel);
        }
    }

    // Update is called once per frame
    void Update()
    {
        
    }
}
