﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GameManager : MonoBehaviour
{
    public GameField gameField;
    public PeopleManager peopleManager;
    public CameraManager cameraManager;

    void Awake()
    {
        cameraManager.Init();
        gameField.Init();
        peopleManager.Init(0); //Must set default for level
    }
    
    void Update()
    {
        
    }
}
