﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PeopleManager : MonoBehaviour
{
    public int CountOfPeople;
    public int UnusedPeople;

    public void Init(int startCount)
    {
        CountOfPeople = startCount;
        UnusedPeople = 0;
    }

    public void ChangeCount(int diff)
    {
        CountOfPeople += diff;
    }

    public void ChangeUnused(int diff)
    {
        UnusedPeople += diff;
    }
}
