﻿using System.Collections;
using System.Collections.Generic;
using System.IO.IsolatedStorage;
using UnityEditor;
using UnityEngine;
using System;

public abstract class Building : MonoBehaviour
{
    public string Name;

    public bool IsDone;
    public float TimeToFinish;

    public bool Full => AssignedPeople < MaxPeople;
    public bool Empty => AssignedPeople == 0;

    public int AssignedPeople;
    public int MaxPeople;

    public float BuildComplexity;
    public float ActionComplexity;

    public bool ChangePeopleCount(int diff)
    {
        if (AssignedPeople + diff <= MaxPeople && AssignedPeople + diff >= 0)
            AssignedPeople += diff;
        else
            return false;

        return true;
    }

    public int UpdateState()
    {
        float complexity = 0.0f;

        if (!IsDone)
        {
            Build();
            complexity = BuildComplexity;
        }
        else
        {
            Action();
            complexity = ActionComplexity;
        }

        System.Random randomizer = new System.Random();
        int deadPeople = 0;

        for (int i = 0; i < AssignedPeople; i++)
        {
            if (randomizer.Next(0, 100) / 100.0f < complexity)
            {
                deadPeople++;
            }
        }

        return deadPeople;
    }

    public void Build()
    {
        //Add calculate by assignedPeople
        TimeToFinish -= 0.1f;

        if (TimeToFinish < 0.0f)
        {
            IsDone = true;
        }
            
    }

    public abstract void Action();
}
