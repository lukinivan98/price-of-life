﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Stonecutter : Building
{
    public Stonecutter()
    {
        Name = "Stonecutter";

        IsDone = false;
        TimeToFinish = 20.0f;

        MaxPeople = 10;

        BuildComplexity = 0.5f;
        ActionComplexity = 0.5f;
    }

    public override void Action()
    {
        
    }
}
    
