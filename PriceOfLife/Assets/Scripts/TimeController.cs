﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Transactions;
using JetBrains.Annotations;
using UnityEngine;

public class TimeController : MonoBehaviour
{
    private int currentTime;

    //Out of class?
    enum MonthName
    {
        January,
        February,
        March,
        April,
        May,
        June,
        July,
        August,
        September,
        October,
        November,
        December
    }

    public int Month => currentTime % 12 + 1;

    public int Year => currentTime / 12;

    public TimeController(int time)
    {
        currentTime = time;
    }

    public void IncreaseTime()
    {
        currentTime++;
    }
}
