﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraManager : MonoBehaviour
{
    bool isRotating = false;
    Vector3 currentRotation;
    Vector3 rotationPerFrame;
    public Camera camera;
    Transform objectOnFocus;

    public void Init()
    {
        isRotating = true;
        currentRotation = new Vector3(0f, 0f, 0f);
        rotationPerFrame = new Vector3(0f, 0.1f, 0f);
    }

    void OnClick()
    {
        if (Input.GetMouseButtonDown(0))
        {
            if(objectOnFocus != null)
            {
                objectOnFocus.localScale = objectOnFocus.localScale - new Vector3(0.1f, 1f, 0.1f);
            }
            
            RaycastHit hit;
            Ray ray = camera.ScreenPointToRay(Input.mousePosition);
            if (Physics.Raycast(ray, out hit))
            {
                objectOnFocus = hit.transform;
                objectOnFocus.localScale = objectOnFocus.localScale + new Vector3(0.1f, 1f, 0.1f);
                // Do something with the object that was hit by the raycast.
            }
        }
    }

    void Update()
    {
        if (isRotating)
        {
            transform.Rotate(rotationPerFrame);
        }
        OnClick();
    }
}
