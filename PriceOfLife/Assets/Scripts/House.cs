﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class House : Building
{
    public House()
    {
        Name = "House";

        IsDone = false;
        TimeToFinish = 5.0f;

        MaxPeople = 5;

        BuildComplexity = 0.25f;
        ActionComplexity = 1.0f;
    }

    public override void Action()
    {

    }
}
