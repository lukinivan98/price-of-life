﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GameField : MonoBehaviour
{
    public List<CellForBuilding> cells;
    public Tower tower;
    public CellForBuilding cellPrefab;

    public void Init()
    {
        tower.Init();
    }
    
    void Update()
    {
        
    }
}
